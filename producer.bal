import ballerinax/kafka;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    string message = "dsa-assigment.hafeni";
    // Sends the message to the Kafka topic.
    check kafkaProducer->send({
                                topic: "dsa-assigment.hafeni",
                                value: message.toBytes() });

    // Flushes the sent messages.
    check kafkaProducer->'flush();
}
