import ballerina/http;
import ballerina/io;

# A service representing a network-accessible API
# bound to absolute path `/kafka` and port `9090`.
service on new http:Listener(9092), new http:Listener(9093), new http:Listener(9094)  {

    # A resource respresenting an invokable API method
    # accessible at `/kafka/`.
    #
    # + caller - the client invoking this resource
    # + request - the inbound request
    resource function get topics(http:Caller caller, http:Request request) {

        // Send a response back to the caller.
        error? result = caller->respond("dsa-assigment.hafeni");
        if (result is error) {
            io:println("Error in responding: ", result);
        }
    }
}
